
//const { request, response } = require('express')
const express =require('express')

const mysql =require('mysql')

const app =express()

function openDatabaseConnection(){

    const connection =mysql.createConnection({

        host :'localhost',
        port:3306,
        user:'root',
        password :'root',

        database:'react'

    })
    connection.connect()
    return connection
}

app.use(express.json())   //to parse

//find students by roll_no
app.get('/Student/:roll_no', (request, response) => {
    const { roll_no } = request.params
  const connection = openDatabaseConnection()
  const statement = `select  roll_no,name, Class, division, dateofbirth,parent_mobile_no
   from Student where roll_no=${roll_no}`
  connection.query(statement, (error, data) => {
  connection.end()
  
    if (error) {
    
      response.send(error)
    } else {  
      response.send(data)
    }
  })
  })

app.listen(4000,()=>
{
    console.log(`server started on port 4000`)
})  

//Add Student

app.post('/student/',(request,response)=>{

   const  {roll_no,name,Class,division,dateofbirth,parent_mobile_no} =request.body
   
   // const {roll_no,name ,class, division,dateofbirth,parent_mobile_no }=request.body

    const connection =openDatabaseConnection()

    const statement = `insert into student (roll_no ,name,Class ,division,dateofbirth,parent_mobile_no)
    values
    ('${roll_no}','${name}','${Class}','${division}','${dateofbirth}' ,'${parent_mobile_no}')`

    connection.query(statement,(error,data)=>{
        connection.end()
        if(error)
        {
            response.send(error)
        }else{
            response.send(data)

        }
    })
    
})

//update class and division for specific student  ..here first find by Id and change/update the field
// Use put method for update
app.put('/student/:roll_no',(request,response)=>{
    const{Class,division} =request.body
    const {roll_no} =request.params
    const connection =openDatabaseConnection()

    const statement =`update student set Class='${Class}' ,
    division ='${division}' where roll_no=${roll_no}`
    connection.query(statement,(error,data)=>{
        connection.end()
        if(error)
        {
            response.send(error)
        }else{
            response.send(data)
        }
    }) 

})

// delete student object/row

app.delete('/student/:roll_no',(request,response)=>
{
    const {roll_no} =request.params
    const connection =openDatabaseConnection()
    const statement =`delete from student where roll_no=${roll_no}`
    connection.query(statement,(error,data)=>
    {
        connection.end()
        if(error)
        {
            response.send(error)
        }else{
            response.send(data)

        }
    })
})

//  Fetch all students of particular class 

app.get('/student_a/:Class',(request,response)=>{
    const {Class} =request.params
    const connection= openDatabaseConnection()
    const statement =`select * from student where Class='${Class}'`
    connection.query(statement,(error,data)=>{
        connection.end()
     if(error){
         response.send(error)
     }else{
        response.send(data)
     }
     
    })
})

//   Fetch all students of particular birth year 

app.get('/student_dob/:dateofbirth',(request,response)=>{
    const connection =openDatabaseConnection()
     const statement = `select * from student where year(dateofbirth)='${request.params.dateofbirth}'`

     connection.query(statement,(error,data)=>{
         connection.end()
         if(error){
             response.send(error)
         }else{
             response.send(data)
         }

     })
})